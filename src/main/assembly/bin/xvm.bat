@setlocal
@echo off
rem ===
rem Copyright (c) 2018 Neeve Research, LLC. All Rights Reserved.
rem Confidential and proprietary information of Neeve Research, LLC.
rem CopyrightVersion 1.0
rem ===

rem Switch to installation root
pushd %~dp0\..
echo %CD%
set STATUS=0

rem Validate that JAVA_HOME is set
if not defined JAVA_HOME goto NO_JAVA
set JAVA_HOME=%JAVA_HOME:"=%

rem Display help if no args
if "%~1"=="" goto HELP

rem Pre-parse args to find the XVM name
set XVM=default
set ARGS=%*
:PREPARSE_ARGS
if "%~1" NEQ "" ( 
  if "%~1"=="-n" (
    set XVM=%~2
  )
  if "%~1"=="--name" (
    set XVM=%~2
  )
  shift
  goto PREPARSE_ARGS
)

rem Setup JVM Args
set GC_OPTS=-Xms1572m -Xmx1572m

rem Make sure log directory exists
if not exist rdat\logs (
  mkdir rdat\logs
)

rem Launch the XVM
"%JAVA_HOME%\bin\java" %GC_OPTS% -Dnv.native.suppressextraction=true -cp "libs\*" com.neeve.server.Main %ARGS% 
set STATUS=%ERRORLEVEL%
goto DONE

:HELP
"%JAVA_HOME%\bin\java.exe" -cp "libs\*" com.neeve.server.Main --help
set STATUS=%ERRORLEVEL%
goto DONE

:NO_JAVA
echo The JAVA_HOME environment variable needs to be set.
set STATUS=1
goto DONE

:DONE
@popd
@endlocal
exit /B %STATUS%