package com.freeingreturns.service.txmanager.driver;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import static java.text.MessageFormat.format;

import com.freeingreturns.roe.txmanager.RplyTxmCreateGroup;
import com.freeingreturns.roe.txmanager.RqstTxmCreateGroup;
import com.freeingreturns.roe.txmanager.TxGroupDTO;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Argument;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppMain;
import com.neeve.server.app.annotations.AppStat;
import com.neeve.stats.IStats.Counter;
import com.neeve.stats.StatsFactory;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;
import com.neeve.util.UtlGovernor;

/**
 * A test driver app for the Application.
 */
public class BasicFlow {
    private Tracer logger = Tracer.create("service.txmanager", Level.FINEST);
    @Configured(property = "service.txmanager.tests.basic.autoStart")
    private boolean autoStart;

    @Configured(property = "service.txmanager.tests.basic.sendCount")
    private int sendCount;

    @Configured(property = "service.txmanager.tests.basic.sendRate")
    private int sendRate;

    @Configured(property = "service.txmanager.channels.txRequestChannel", defaultValue = "TxRequestChannel")
    private String rqstChannelTx;

    @AppStat
    private final Counter sentCount = StatsFactory.createCounterStat("Basic TxFlow Count");

    private volatile AepEngine engine;

    private final AtomicReference<Thread> sendingThread = new AtomicReference<Thread>();
    private volatile AepMessageSender messageSender;
    private CountDownLatch msgLatch;

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @AppInjectionPoint
    final public void setAepEngine(AepEngine engine) {
        this.engine = engine;
    }

    /**
     * Starts sending messages (in a background thread)
     */
    @Command(name = "send", displayName = "Send Messages", description = "Instructs the driver to send messages")
    public final void doSend(@Argument(name = "count", position = 1, required = true, description = "The number of messages to send") final int count,
                             @Argument(name = "rate", position = 2, required = true, description = "The rate at which to send") final int rate) throws Exception {

        // loopback only
        engine.waitForMessagingToStart();

        msgLatch = new CountDownLatch(count);

        final Thread thread = new Thread(new SendingThread(rate), "TxProcessor Send Driver");

        Thread oldThread = sendingThread.getAndSet(thread);
        if (oldThread != null) {
            oldThread.interrupt();
            oldThread.join();
        }
        thread.start();
    }

    /**
     * Stops the current sending thread (if active_ 
     */
    @Command(name = "stopSending", displayName = "Stop Sending", description = "Stops sending of messages.")
    final public void stop() throws Exception {
        Thread oldThread = sendingThread.getAndSet(null);
        if (oldThread != null) {
            oldThread.join();
        }
    }

    /**
     * Gets the number of messages sent by the sender. 
     * 
     * @return The number of messages sent by this sender.
     */
    @Command
    public long getSentCount() {
        return sentCount.getCount();
    }

    @AppMain
    public void run(String[] args) throws Exception {
        if (autoStart) {
            doSend(sendCount, sendRate);
        }
    }

    @EventHandler
    public final void onEvent(RplyTxmCreateGroup message) {

        logger.log(format("Matched add-message:{0}, value:{1}", message.getMsgId()), Level.INFO);
        msgLatch.countDown();
        return;
    }

    public final CountDownLatch getMsgLatch() {
        return msgLatch;
    }

    public class SendingThread implements Runnable {

        private int sent = 0;
        private int rate;

        DecimalFormat fmt = new DecimalFormat("###");
        final Random rand = new Random();

        public SendingThread(int rt) {
            rate = rt;
        }

        @Override
        public void run() {
            final UtlGovernor sendGoverner = new UtlGovernor(rate);
            RqstTxmCreateGroup message = RqstTxmCreateGroup.create();
            message.setMsgId(System.currentTimeMillis());
            TxGroupDTO dto = TxGroupDTO.create();
            dto.setCustomerID("CT_" + fmt.format(rand.nextInt(1000)));
            dto.setRetailStoreID("RS_" + fmt.format(rand.nextInt(1000)));
            dto.setWorkstationID("015");
            dto.setEmployeeID("EM_" + fmt.format(rand.nextInt(1000)));
            dto.setBusinessDay("20180806");
            dto.setTransactionSequenceNumber(1);
            dto.setReturnTicketNumber("");
            message.setTxGroupDTO(dto);

            messageSender.sendMessage(rqstChannelTx, message);
            sentCount.increment();
            sendGoverner.blockToNext();

            System.out.println("Chnel:" + rqstChannelTx + ", SENT:" + message.toString());
        }
    }

}
