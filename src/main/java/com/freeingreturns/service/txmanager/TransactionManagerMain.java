package com.freeingreturns.service.txmanager;

import static java.text.MessageFormat.format;

import java.util.Iterator;
import java.util.Set;

import com.freeingreturns.roe.customer.RplyCustomerQuery;
import com.freeingreturns.roe.customer.RqstCustomerQuery;
import com.freeingreturns.roe.product.RplyLineItemQuery;
import com.freeingreturns.roe.product.RqstLineItemQuery;
import com.freeingreturns.roe.txmanager.RplyTxmCreateGroup;
import com.freeingreturns.roe.txmanager.RplyTxmGroupCount;
import com.freeingreturns.roe.txmanager.RplyTxmItemAdd;
import com.freeingreturns.roe.txmanager.RplyTxmModifyGroup;
import com.freeingreturns.roe.txmanager.RplyTxmQueryGroup;
import com.freeingreturns.roe.txmanager.RplyTxmQueryItem;
import com.freeingreturns.roe.txmanager.RqstTxmCreateGroup;
import com.freeingreturns.roe.txmanager.RqstTxmGroupCount;
import com.freeingreturns.roe.txmanager.RqstTxmItemAdd;
import com.freeingreturns.roe.txmanager.RqstTxmModifyGroup;
import com.freeingreturns.roe.txmanager.RqstTxmQueryGroup;
import com.freeingreturns.roe.txmanager.RqstTxmQueryItem;
import com.freeingreturns.roe.txmanager.TxGroupDTO;
import com.freeingreturns.roe.txmanager.TxItemDTO;
import com.freeingreturns.roe.vendor.RplyRetailStoreQuery;
import com.freeingreturns.roe.vendor.RqstRetailStoreQuery;
import com.freeingreturns.service.txmanager.state.ExtGroupQry;
import com.freeingreturns.service.txmanager.state.ExtItemQry;
import com.freeingreturns.service.txmanager.state.Repository;
import com.freeingreturns.service.txmanager.state.TxGroup;
import com.freeingreturns.service.txmanager.state.TxItem;
import com.freeingreturns.service.txmanager.state.TxLineItemMap;
import com.freeingreturns.utils.FRLogger;
import com.freeingreturns.utils.FRUnhandleExcpetionCatcher;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.IAepApplicationStateFactory;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.aep.event.AepEngineStoppingEvent;
import com.neeve.aep.event.AepMessagingStartedEvent;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.event.alert.IAlertEvent;
import com.neeve.rog.log.RogLog;
import com.neeve.rog.log.RogLogCdcProcessor;
import com.neeve.server.app.annotations.AppEventHandlerContainersAccessor;
import com.neeve.server.app.annotations.AppHAPolicy;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppStateFactoryAccessor;
import com.neeve.sma.MessageView;
import com.neeve.trace.Tracer.Level;

@AppHAPolicy(value = AepEngine.HAPolicy.StateReplication)
public class TransactionManagerMain extends MsgLogger {

    public static final int NUM_OF_RESPONSES_GROUP_QUERY = 2;

    private CDCTxmHandler handler;
    private RogLog log;
    private RogLogCdcProcessor processor;

    private AepEngine aepEngine;
    private AepMessageSender messageSender;

    @Configured(property = "service.txmanager.channels.vendorRequestChannel", defaultValue = "VendorRequestChannel")
    private String rqstChannelVendor;

    @Configured(property = "service.txmanager.channels.productRequestChannel", defaultValue = "ProductRequestChannel")
    private String rqstChannelProduct;

    @Configured(property = "service.txmanager.channels.customerRequestChannel", defaultValue = "CustomerRequestChannel")
    private String rqstChannelCustomer;

    @Configured(property = "service.txmanager.channels.txReplyChannel", defaultValue = "TxReplyChannel")
    private String txReplyChannel;

    private UnhandledMessages ignoredMessages;

    @Configured(property = "service.txmanager.responseValueOnLookupFail", defaultValue = "false")
    private boolean failGroupLookupResponseVal;





    public TransactionManagerMain() {
        Thread.setDefaultUncaughtExceptionHandler(FRUnhandleExcpetionCatcher.getInstance());
    }





    @AppStateFactoryAccessor
    final public IAepApplicationStateFactory getStateFactory() {
        return new IAepApplicationStateFactory() {
            @Override
            final public Repository createState(MessageView view) {
                return Repository.create();
            }
        };
    }





    @AppInjectionPoint
    public void injectAepEngine(AepEngine engine) {
        aepEngine = engine;
    }





    @EventHandler
    final public void onMessagingStarted(final AepMessagingStartedEvent event) {
        Thread t = new Thread(new Runnable() {
            public void run() {
                FRLogger.logAsIs(Level.INFO, "Engine activated, starting CDC");
                try {
                    startCdc();
                }
                catch (Exception e_) {
                    e_.printStackTrace();
                }
            }
        });
        t.start();
    }





    @EventHandler
    public void onEngineStopped(AepEngineStoppingEvent event) {
        FRLogger.logAsIs(Level.INFO, "Engine stopping, stopping CDC");
        try {
            stopCdc();
        }
        catch (Exception e_) {
            e_.printStackTrace();
        }
    }





    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender messageSender) {
        this.messageSender = messageSender;
    }





    @AppEventHandlerContainersAccessor
    public void getEventHandlers(Set<Object> handlers) {
        ignoredMessages = new UnhandledMessages();
        handlers.add(ignoredMessages);
    }





    @Command(name = "Start CDC")
    public void startCdc() throws Exception {
        Boolean enabled = false;
        try {
            enabled = Boolean.valueOf(aepEngine.getStore().getPersister().getDescriptor().getProperty(RogLog.PROP_CDC_ENABLED));
            if (enabled) {
                log = (RogLog)aepEngine.getStore().getPersister();
                handler = new CDCTxmHandler();
                processor = log.createCdcProcessor(handler);
                processor.run();
                return;
            }
        }
        catch (Exception e) {
        }

        FRLogger.logAsIs(Level.WARNING, "CDC Storage disabled");
    }





    @Command(name = "Stop CDC")
    public void stopCdc() throws Exception {
        if (processor != null)
            processor.close();
    }





    @EventHandler
    final public void onMessage(RqstTxmCreateGroup msg, Repository repo) {
        FRLogger.logMethodCall();

        //
        // make sure someone has not already asked me for this query
        //
        ExtGroupQry qry = repo.getExtQueries().get(msg.getMsgId());
        if (qry  != null) {
            FRLogger.logfmt(Level.SEVERE,
                            msg.getMsgId(),
                            msg.getMessageChannel(),
                            "Mesage group with this ID has already been created! CmpId: {0}",
                            qry.getCompoundTransactionId());
            txGroupFail(msg.getMsgId(),
                        msg.getMessageChannel(),
                        qry.getCompoundTransactionId(),
                        RplyTxmCreateGroup.create(),
                        msg.getTxGroupDTO());
            return;
        }

        //
        // i look ok, find the retail-transaction (the group)
        // and make sure no one has tried to creat this one before
        //
        TxGroupDTO srcDto = msg.getTxGroupDTO();
        String cmpId = getCompoundId(srcDto.getRetailStoreID(),
                                     srcDto.getBusinessDay(),
                                     srcDto.getWorkstationID(),
                                     srcDto.getTransactionSequenceNumber());
        TxGroup txG = repo.getTxGroups().get(cmpId);
        if (txG != null) {
            txGroupFail(msg.getMsgId(),
                        msg.getMessageChannel(),
                        cmpId,
                        RplyTxmCreateGroup.create(),
                        msg.getTxGroupDTO());
            return;
        }

        //
        // create external system references
        //
        RqstCustomerQuery custQ = RqstCustomerQuery.create();
        RqstRetailStoreQuery vendorQ = RqstRetailStoreQuery.create();
        initRefDataRequests(custQ, vendorQ, msg);
        FRLogger.logfmt(Level.INFO,
                        msg.getMsgId(),
                        msg.getMessageChannel(),
                        "Sending orchestration messages. Channels:{0}, {1}",
                        rqstChannelVendor,
                        rqstChannelCustomer);
        messageSender.sendMessage(rqstChannelVendor, vendorQ);
        messageSender.sendMessage(rqstChannelCustomer, custQ);

        //
        // add my group into the map
        //
        TxGroupDTO dstDto = new TxGroupDTO();
        txG = TxGroup.create();
        srcDto.copyInto(dstDto);
        txG.setTxGroupDTO(dstDto);
        txG.setIsValidGroup(false);
        txG.setCompoundTransactionId(cmpId);
        repo.getTxGroups().put(cmpId, txG);

        //
        // keep track of my external queries
        //
        qry = ExtGroupQry.create();
        qry.setMsgId(msg.getMsgId());
        qry.setTotalNumberOfReplies(0);
        qry.setCompoundTransactionId(cmpId);
        repo.getExtQueries().put(qry.getMsgId(), qry);

        FRLogger.logfmt(Level.INFO,
                        msg.getMsgId(),
                        msg.getMessageChannel(),
                        "Added transaction group: {0}, msgid: {1}",
                        cmpId,
                        Long.toString(msg.getMsgId()));
        
        //aepEngine.injectMessage(message, nonBlocking, delay);
    }





    @EventHandler
    final public void onMessage(RqstTxmQueryGroup msg, Repository repo) {
        RplyTxmQueryGroup rply = RplyTxmQueryGroup.create();
        rply.setMsgId(msg.getMsgId());

        String cmpId = getCompoundId(msg.getRetailStoreID(),
                                     msg.getBusinessDay(),
                                     msg.getWorkstationID(),
                                     msg.getTransactionSequenceNumber());
        TxGroup txG = repo.getTxGroups().get(cmpId);
        if (txG == null) {
            rply.setFound(false);
            TxGroupDTO grp = TxGroupDTO.create();
            grp.setCompoundTransactionId(cmpId);
            grp.setRetailStoreID(msg.getRetailStoreID());
            grp.setBusinessDay(msg.getBusinessDay());
            grp.setWorkstationID(msg.getRetailStoreID());
            grp.setTransactionSequenceNumber(msg.getTransactionSequenceNumber());
            rply.setTxGroupDTO(grp);
            rply.addTxItems(TxItemDTO.create());
            messageSender.sendMessage(txReplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "cannot find txgroup id:{0}", cmpId);
            return;
        }

        TxGroupDTO txgDst = TxGroupDTO.create();
        txG.getTxGroupDTO().copyInto(txgDst);
        rply.setTxGroupDTO(txgDst);
        FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                        "query found txgroup id: {0}", cmpId);

        TxLineItemMap txMap = txG.getTransactions();
        Iterator<Long> itr = txMap.keySet().iterator();
        TxItem item;
        Long key;
        TxItemDTO itmDst;
        while (itr.hasNext()) {
            key = itr.next();
            item = txMap.get(key);
            if (item != null) {
                itmDst = TxItemDTO.create();
                item.getTxItemDTO().copyInto(itmDst);
                rply.addTxItems(itmDst);
                FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                                "  + added to query tx: {1}, item-id: {0}", itmDst.getItemID(), cmpId);
            }
        }

        rply.setFound(true);
        messageSender.sendMessage(txReplyChannel, rply);
        return;
    }





    @EventHandler
    final public void onMessage(RqstTxmQueryItem msg, Repository repo) {
        RplyTxmQueryItem rply = RplyTxmQueryItem.create();
        rply.setMsgId(msg.getMsgId());

        String cmpId = getCompoundId(msg.getRetailStoreID(),
                                     msg.getBusinessDay(),
                                     msg.getWorkstationID(),
                                     msg.getTransactionSequenceNumber());

        TxGroup txG = repo.getTxGroups().get(cmpId);
        if (txG == null) {
            rply.setFound(false);
            TxGroupDTO grp = TxGroupDTO.create();
            grp.setCompoundTransactionId(cmpId);
            grp.setRetailStoreID(msg.getRetailStoreID());
            grp.setBusinessDay(msg.getBusinessDay());
            grp.setWorkstationID(msg.getRetailStoreID());
            grp.setTransactionSequenceNumber(msg.getTransactionSequenceNumber());
            rply.setTxGroupDTO(grp);
            rply.setTxItem(TxItemDTO.create());
            messageSender.sendMessage(txReplyChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "cannot find txgroup id: {0}", cmpId);
            return;
        }

        TxLineItemMap txMap = txG.getTransactions();
        TxItem item = txMap.get(msg.getRetailTransactionLineItemSequenceNumber());
        if (item != null) {
            TxGroupDTO txgDst = TxGroupDTO.create();
            txG.getTxGroupDTO().copyInto(txgDst);
            rply.setTxGroupDTO(txgDst);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "query found txgroup id:{0}", cmpId);

            TxItemDTO itmDst = TxItemDTO.create();
            item.getTxItemDTO().copyInto(itmDst);
            rply.setTxItem(itmDst);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "  + added to query tx: {1}, item-id: {0}", itmDst.getItemID(), cmpId);
            rply.setFound(true);
        }

        messageSender.sendMessage(txReplyChannel, rply);
        return;
    }





    /**
     * 
     */
    private void initRefDataRequests(RqstCustomerQuery custQ, RqstRetailStoreQuery vendorQ, RqstTxmCreateGroup msg) {
        if (msg.getTxGroupDTO() == null) {
            FRLogger.logfmt(Level.SEVERE, custQ.getMsgId(), custQ.getMessageChannel(),
                            "No DTO object available to xfer customer info from!");
            return;
        }
        
        custQ.setMsgId(msg.getMsgId());
        vendorQ.setMsgId(msg.getMsgId());

        custQ.setCustomerID(msg.getTxGroupDTO().getCustomerID());
        vendorQ.setRetailStoreID(msg.getTxGroupDTO().getRetailStoreID());
    }





    @EventHandler
    final public void onInternalMessage(RplyRetailStoreQuery msg, Repository repo) {
        FRLogger.logMethodCall();

        //
        // lookup the external query
        //
        ExtGroupQry extQry = repo.getExtQueries().get(msg.getMsgId());
        if (extQry == null) {
            // this could be a normal user query response (not originated by me)
            logExtQryLookupFailed(msg.getMsgId(), msg.getMessageChannel(), msg.getClass(),
                                  msg.getRetailStore().getRetailStoreID());
            return;
        }

        //
        // i found the query, but I was not able to find the store
        // this transaction is bad!
        //
        if (!msg.getFound()) {
            txGroupFail(msg.getMsgId(),
                        msg.getMessageChannel(),
                        extQry.getCompoundTransactionId(),
                        RplyTxmCreateGroup.create(),
                        null);
            return;
        }

        //
        // retail store was good
        //
        int ntotal = extQry.getTotalNumberOfReplies() + 1;
        extQry.setTotalNumberOfReplies(ntotal);
        extQry.setValidVendor(true);

        //
        // have all external systems replied?
        //
        logReplyCall(msg.getMsgId(), msg.getMessageChannel(), msg.getRetailStore().getRetailStoreID());
        if (ntotal >= NUM_OF_RESPONSES_GROUP_QUERY) {
            handleExternalSystemsQueryComplete(repo, extQry);
        }

        //
        // am done
        //
        return;

    }





    @EventHandler
    final public void onInternalMessage(RplyCustomerQuery msg, Repository repo) {
        FRLogger.logMethodCall();

        //
        // lookup the external query
        //
        ExtGroupQry extQry = repo.getExtQueries().get(msg.getMsgId());
        if (extQry == null) {
            logExtQryLookupFailed(msg.getMsgId(), msg.getMessageChannel(), msg.getClass(),
                                  msg.getCustomerID());
            return;
        }

        //
        // was I able to find the customer?
        //
        if (!msg.getFound()) {
            txGroupFail(msg.getMsgId(),
                        msg.getMessageChannel(),
                        extQry.getCompoundTransactionId(),
                        RplyTxmCreateGroup.create(),
                        null);
            return;
        }

        //
        // customer was good
        //
        int ntotal = extQry.getTotalNumberOfReplies() + 1;
        extQry.setTotalNumberOfReplies(ntotal);
        extQry.setValidCustomer(true);

        //
        // have all external systems replied?
        //
        logReplyCall(msg.getMsgId(), msg.getMessageChannel(), msg.getCustomerID());
        if (ntotal >= NUM_OF_RESPONSES_GROUP_QUERY) {
            handleExternalSystemsQueryComplete(repo, extQry);
        }

        //
        // am done
        //
        return;
    }





    private void handleExternalSystemsQueryComplete(Repository repo, ExtGroupQry extQry) {
        TxGroupDTO dstInfo = new TxGroupDTO();
        RplyTxmCreateGroup successMsg = RplyTxmCreateGroup.create();
        TxGroup txG = repo.getTxGroups().get(extQry.getCompoundTransactionId());

        txG.setIsValidGroup(true);
        txG.getTxGroupDTO().copyInto(dstInfo);
        successMsg.setMsgId(extQry.getMsgId());
        successMsg.setSucceeded(true);
        successMsg.setTxGroupDTO(dstInfo);

        messageSender.sendMessage(this.txReplyChannel, successMsg);
        repo.getExtQueries().remove(extQry.getMsgId());

        FRLogger.logfmt(Level.FINE, extQry.getMsgId(), "--",
                        "External systems-query complete. [{0}]",
                        extQry.getCompoundTransactionId());
    }





    @EventHandler
    final public void onMessage(RqstTxmItemAdd msg, Repository repo) {
        FRLogger.logMethodCall();

        //
        // make sure it's a valid group
        //
        TxItemDTO srcDto = validateDTO(msg);
        if (srcDto == null)
            return;

        //
        // as long as I can find the tx-group, I don't need to validate
        // anything else
        //
        String cmpId = msg.getTxItemDTO().getCompoundTransactionId();
        TxGroup txG = repo.getTxGroups().get(cmpId);
        if (txG == null) {
            cmpId = getCompoundId(srcDto.getRetailStoreID(),
                                  srcDto.getBusinessDay(),
                                  srcDto.getWorkstationID(),
                                  srcDto.getTransactionSequenceNumber());
            txG = repo.getTxGroups().get(cmpId);
            if (txG == null) {
                txItemFail(msg.getMsgId(), msg.getMessageChannel(), cmpId, srcDto);
                FRLogger.logfmt(Level.INFO,
                                msg.getMsgId(),
                                msg.getMessageChannel(),
                                "ERROR - tx-group query fail. cmpid: {0}, itemid: {1}", 
                                cmpId, msg.getTxItemDTO().getItemID());
                return;
            }
        }

        //
        // this group has not been validated
        //
        if (!txG.isValid()) {
            txItemFail(msg.getMsgId(), msg.getMessageChannel(), cmpId, srcDto);
            FRLogger.logfmt(Level.INFO,
                            msg.getMsgId(),
                            msg.getMessageChannel(),
                            "ERROR - attempting to add a tx-line-itme to an invalid tx-group. cmpid: {0}, , itemid: {1}", 
                            cmpId, msg.getTxItemDTO().getItemID());
            return;
        }

        //
        // make sure I do not have this in my repo
        //
        TxLineItemMap tMap = txG.getTransactions();
        TxItem item = tMap.get(srcDto.getRetailTransactionLineItemSequenceNumber());
        if (item != null) {
            txItemFail(msg.getMsgId(), msg.getMessageChannel(), cmpId, srcDto);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "ERROR - tx-item already exists for that group. cmpid: {0}, tx-line-item-seq-no: {1}", 
                            cmpId, srcDto.getRetailTransactionLineItemSequenceNumber());

            return;
        }

        //
        // make sure it's a valid ID
        //
        storeExtReqIntoMap(msg, repo, txG);
        requestValidationFromProductCatalog(msg, repo);
    }





    @EventHandler
    final public void onMessage(RqstTxmModifyGroup msg, Repository repo) {
        RplyTxmModifyGroup rply = RplyTxmModifyGroup.create();
        rply.setMsgId(msg.getMsgId());
        rply.setTxGroupDTO(msg.takeTxGroupDTO());
        String cmpId = msg.getTxGroupDTO().getCompoundTransactionId();

        if (rply.getTxGroupDTO().getReturnTicketNumber() == null) {
            rply.setSucceeded(false);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "return ticket is NULL. cmp-id: {0}, fullmsg: {1}", cmpId, msg.toString());
            messageSender.sendMessage(txReplyChannel, rply);
            return;
        }

        TxGroup txG = repo.getTxGroups().get(cmpId);
        if (txG == null) {
            rply.setSucceeded(false);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "unable to update tx-group. cmp-id: {0}", cmpId);
            messageSender.sendMessage(txReplyChannel, rply);
            return;
        }

        txG.getTxGroupDTO().setReturnTicketNumber(msg.getTxGroupDTO().getReturnTicketNumber());
        TxItemDTO[] items = msg.getTxItems();
        double totalTransactioNet = txG.getTransactionNetTotal();
        double origNT = totalTransactioNet;
        int txLen = 0;
        if (items != null) {
            txLen = items.length;
            for (TxItemDTO item : items) {
                totalTransactioNet -= (item.getPermanentRetailPriceAtTimeOfSale() * item.getSaleReturnLineItemQuantity());
            }
        }
        txG.setTransactionNetTotal(totalTransactioNet);
        rply.setSucceeded(true);
        FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                        "succeeded in altering tx-group. cmp-id: {0}, #Txs: {3}, orig-net-total: {1}, new net-total: {2}",
                        cmpId, origNT, totalTransactioNet, txLen);
        messageSender.sendMessage(txReplyChannel, rply);
    }





    @EventHandler
    final public void onMessage(RqstTxmGroupCount msg, Repository repo) {
        RplyTxmGroupCount rply = RplyTxmGroupCount.create();
        rply.setMsgId(msg.getMsgId());
        rply.setRetailStoreID(msg.getRetailStoreID());
        int sz = repo.getTxGroups().keySet().size();
        if (sz < 0)
            sz = 0;
        rply.setCount(sz);
        rply.setFound(true);
        messageSender.sendMessage(this.txReplyChannel, rply);
        FRLogger.logfmt(Level.FINE, rply.getMsgId(), rply.getMessageChannel(),
                        "group count reqeust, id: {0}, count: {1}",
                        msg.getRetailStoreID(),
                        sz);
    }





    /**
     * 
     */
    private TxItemDTO validateDTO(RqstTxmItemAdd msg) {
        TxItemDTO srcDto = msg.getTxItemDTO();
        if (srcDto == null) {
            RplyTxmItemAdd rply = RplyTxmItemAdd.create();
            rply.setMsgId(msg.getMsgId());
            rply.setSucceeded(false);
            txGroupFail(msg.getMsgId(), msg.getMessageChannel(), "UNK", rply, srcDto);
            FRLogger.logfmt(Level.INFO,
                            msg.getMsgId(),
                            msg.getMessageChannel(),
                            "ERROR - tx-group query fail. cmpid: {0}", "UNK");
            return null;

        }
        return srcDto;
    }





    /**
     * 
     */
    private void requestValidationFromProductCatalog(RqstTxmItemAdd msg, Repository repo) {
        RqstLineItemQuery qry = RqstLineItemQuery.create();
        qry.setMsgId(msg.getMsgId());
        qry.setItemID(msg.getTxItemDTO().getItemID());
        qry.setRetaiStoreId(msg.getTxItemDTO().getRetailStoreID());

        messageSender.sendMessage(rqstChannelProduct, qry);
        FRLogger.logfmt(Level.FINE, qry.getMsgId(), qry.getMessageChannel(),
                        "query for product sent, id: {0}",
                        msg.getTxItemDTO().getItemID());
    }





    /**
     * 
     */
    private void storeExtReqIntoMap(RqstTxmItemAdd msg, Repository repo, TxGroup txG) {
        ExtItemQry extItmQry = ExtItemQry.create();
        extItmQry.setMsgId(msg.getMsgId());
        extItmQry.setCompoundTransactionId(txG.getCompoundTransactionId());
        extItmQry.setValidProduct(false); // fals until notified from product catalog
        extItmQry.setTotalNumberOfReplies(0); // no-one has called back yet
        TxItemDTO srcDto = msg.getTxItemDTO();
        TxItemDTO dstDto = TxItemDTO.create();
        if (srcDto != null)
            srcDto.copyInto(dstDto);
        extItmQry.setTxItemDTO(dstDto);
        repo.getExtPrdQueries().put(msg.getMsgId(), extItmQry);
    }





    private String getCompoundId(String storeId, String businessDay, String wsid, long grpTxId) {
        return format("{0}:{1}:{2}:{3}", storeId, businessDay, wsid, Long.toString(grpTxId));
    }





    @EventHandler
    final public void onRplyLineItemQuery(RplyLineItemQuery msg, Repository repo) {
        FRLogger.logMethodCall();

        //
        // get my stored qeury
        //
        ExtItemQry extItmQry = repo.getExtPrdQueries().remove(msg.getMsgId());
        if (extItmQry == null) {
            txItemFail(msg.getMsgId(), msg.getMessageChannel(), "UNK", null);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "ERROR - tx-item query failed. Cannot find the ExtItemQuery Record!. msgid: {0}, itmid: {1}",
                            Long.toString(msg.getMsgId()),
                            msg.getItemID());
            return;
        }

        //
        // get my transation group
        //
        String cmpId = extItmQry.getCompoundTransactionId();
        TxItemDTO srcDto = extItmQry.getTxItemDTO();
        TxGroup txG = repo.getTxGroups().get(cmpId);
        if (txG == null) {
            txItemFail(msg.getMsgId(), msg.getMessageChannel(), cmpId, srcDto);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "ERROR - tx-group query fail. cannot find compoud tx id - cmpid: {0}, itmid: {1}",
                            cmpId, msg.getItemID());
            return;
        }

        //
        // found?
        //
        if (!msg.getFound()) {
            txItemFail(msg.getMsgId(), msg.getMessageChannel(), cmpId, srcDto);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "ERROR - unable to find product, cmpid: {0}, itemid: {1}",
                            cmpId, extItmQry.getTxItemDTO().getItemID());
            return;
        }

        //
        // log reply
        //
        logReplyCall(msg.getMsgId(), msg.getMessageChannel(), msg.getItemID());

        //
        // notify
        //
        RplyTxmItemAdd rply = RplyTxmItemAdd.create();
        rply.setMsgId(msg.getMsgId());
        TxItemDTO dst = TxItemDTO.create();
        srcDto.copyInto(dst);
        rply.setTxItemDTO(dst);
        rply.setSucceeded(true);
        messageSender.sendMessage(txReplyChannel, rply);
        FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                        "added line item to lookup tx-group: {0}, product: {1}",
                        cmpId,
                        msg.getItemID());

        //
        // do the math
        //
        double thisTxTotal = srcDto.getPermanentRetailPriceAtTimeOfSale() * srcDto.getSaleReturnLineItemQuantity();
        txG.setTransactionSalesTotal(txG.getTransactionSalesTotal() + thisTxTotal);
        txG.setTransactionNetTotal(txG.getTransactionNetTotal() + thisTxTotal);

        //
        // save it permanently
        //
        TxItem txitem = TxItem.create();
        TxItemDTO dstP = TxItemDTO.create();
        srcDto.copyInto(dstP);
        txitem.setTxItemDTO(dstP);
        txitem.setTxId(dstP.getRetailTransactionLineItemSequenceNumber());
        txG.getTransactions().put(txitem.getTxId(), txitem);

    }





    @EventHandler
    private final void onApplicationAlert(IAlertEvent alert) {
        FRLogger.logfmt(Level.WARNING, "Application level alert:{0}", alert);
    }





    private void txGroupFail(long msgId, String msgChannel, String cmpId,
                             RplyTxmItemAdd rply,
                             TxItemDTO txItemDto) {
        TxItemDTO dstDto = TxItemDTO.create();
        if (txItemDto != null) {
            txItemDto.copyInto(dstDto);
        }
        else {
            dstDto.setCompoundTransactionId(cmpId);
        }
        rply.setMsgId(msgId);
        rply.setTxItemDTO(dstDto);
        rply.setSucceeded(failGroupLookupResponseVal);
        messageSender.sendMessage(txReplyChannel, rply);
    }





    private void txGroupFail(long msgId, String msgChannel, String cmpId,
                             RplyTxmCreateGroup rply,
                             TxGroupDTO txDto) {
        TxGroupDTO dstDto = TxGroupDTO.create();
        if (txDto != null) {
            txDto.copyInto(dstDto);
        }
        else {
            dstDto.setCompoundTransactionId(cmpId);
        }
        rply.setMsgId(msgId);
        rply.setTxGroupDTO(dstDto);
        rply.setSucceeded(failGroupLookupResponseVal);
        messageSender.sendMessage(txReplyChannel, rply);
    }





    private void txItemFail(long msgId, String msgChannel, String cmpId,
                            TxItemDTO txItemDto) {
        TxItemDTO dstDto = TxItemDTO.create();
        if (txItemDto != null) {
            txItemDto.copyInto(dstDto);
        }
        else {
            dstDto.setCompoundTransactionId(cmpId);
        }
        RplyTxmItemAdd rply = RplyTxmItemAdd.create();
        rply.setMsgId(msgId);
        rply.setTxItemDTO(dstDto);
        rply.setSucceeded(failGroupLookupResponseVal);
        messageSender.sendMessage(txReplyChannel, rply);
    }

}
