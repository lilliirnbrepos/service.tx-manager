/****************************************************************************
 * FILE: UnhandledMessages.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.service.txmanager;

import com.freeingreturns.roe.customer.RplyCustomerAdd;
import com.freeingreturns.roe.customer.RplyCustomerQueryTransactions;
import com.freeingreturns.roe.product.RplyLineItemAdd;
import com.freeingreturns.roe.vendor.RplyEmployeeAdd;
import com.freeingreturns.roe.vendor.RplyEmployeeQuery;
import com.freeingreturns.roe.vendor.RplyRetailStoreAdd;
import com.freeingreturns.roe.vendor.RplyVendorQuery;
import com.freeingreturns.roe.vendor.RplyWorkstationAdd;
import com.freeingreturns.service.txmanager.state.Repository;
import com.freeingreturns.utils.FRLogger;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.sma.event.UnhandledMessageEvent;
import com.neeve.trace.Tracer.Level;

public class UnhandledMessages extends MsgLogger {

    @EventHandler
    public void onUnhandledMessage(UnhandledMessageEvent event) {
        event.setAutoAck(true);
        FRLogger.logfmt(Level.SEVERE, "Unhandled event: {0}/{1}", event.getMessageChannel(), event.getBackingMessage().getMessage());
    }





    @EventHandler
    final public void onRplyVendorQuery(RplyVendorQuery msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyWorkstationAdd(RplyWorkstationAdd msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyRetailStoreAdd(RplyRetailStoreAdd msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyEmployeeAdd(RplyEmployeeAdd msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyCustomerAdd(RplyCustomerAdd msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyCustomerAdd(RplyLineItemAdd msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyCustomerAdd(RplyCustomerQueryTransactions msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }





    @EventHandler
    final public void onRplyEmployeeQuery(RplyEmployeeQuery msg, Repository repo) {
        logIgnoredMessage(msg.getMsgId(), msg.getMessageChannel(), msg.getClass());
        return;
    }
}
