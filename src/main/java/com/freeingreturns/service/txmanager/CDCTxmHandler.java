/****************************************************************************
 * FILE: CDCHandler.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.service.txmanager;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Consumer;

import com.eaio.uuid.UUID;
import com.freeingreturns.roe.txmanager.TxGroupDTO;
import com.freeingreturns.roe.txmanager.TxItemDTO;
import com.freeingreturns.service.txmanager.state.TxGroup;
import com.freeingreturns.service.txmanager.state.TxItem;
import com.freeingreturns.utils.FRLogger;
import com.neeve.rog.IRogChangeDataCaptureHandler;
import com.neeve.rog.IRogNode;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbRetailTransaction;
import freeingreturns.catalogs.orm.DbRetailTransactionLineItem;

public class CDCTxmHandler implements IRogChangeDataCaptureHandler {

    private LocalIMDB localDb;





    public CDCTxmHandler() {
        FRLogger.logAsIs(Level.SEVERE, "CDCCustHandler::CDCCustHandler()");
        localDb = LocalIMDB.getInstance();
        localDb.connectToDb();
    }





    @Override
    public boolean handleChange(UUID objectId, ChangeType changeType, List<IRogNode> entries) {

        if (changeType == ChangeType.Send || changeType == ChangeType.Noop) {
            return true;
        }

        TxItemDTO itmdto;
        TxGroupDTO grpdto;
        DbRetailTransaction txgrp;
        DbRetailTransactionLineItem txitm;
        boolean inserted = false;
        String dbtype ="";
        for (IRogNode node : entries) {

            inserted = false;

            if (node instanceof TxGroup) {
                txgrp = new DbRetailTransaction();
                grpdto = ((TxGroup)node).getTxGroupDTO();
                txgrp.getRetailStoreID().setValue(grpdto.getRetailStoreID());
                txgrp.getWorkstationID().setValue(grpdto.getWorkstationID());
                txgrp.getBusinessDay().setValue(grpdto.getBusinessDay());
                txgrp.getCustomerID().setValue(grpdto.getCustomerID());
                txgrp.getEmployeeID().setValue(grpdto.getEmployeeID());
                txgrp.getTransactionSequenceNumber().setValue(grpdto.getTransactionSequenceNumber());
                txgrp.getReturnTicketNumber().setValue(grpdto.getReturnTicketNumber());
                txgrp.getRecordCreatedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));
                txgrp.getRecordModifiedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));

                if (changeType.equals(ChangeType.Put)) {
                    dbtype = "insert";
                    inserted = localDb.addTransactions(txgrp);
                }
                else if (changeType.equals(ChangeType.Update)) {
                    dbtype = "update";
                    inserted = localDb.updateRetailTransaction(txgrp);
                }
                
                FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, type:{3}, table:{1}, id:{2}",
                                inserted,
                                txgrp.getTableName(),
                                txgrp.getTransactionSequenceNumber().getValue(),
                                dbtype);
            }
            else if (node instanceof TxItem) {
                itmdto = ((TxItem)node).getTxItemDTO();
                txitm = new DbRetailTransactionLineItem();
                txitm.getRetailStoreID().setValue(itmdto.getRetailStoreID());
                txitm.getWorkstationID().setValue(itmdto.getWorkstationID());
                txitm.getBusinessDay().setValue(itmdto.getBusinessDay());
                txitm.getTransactionSequenceNumber().setValue(itmdto.getTransactionSequenceNumber());
                txitm.getPOSDepartmentID().setValue(itmdto.getPosDepartmentID());
                txitm.getItemID().setValue(itmdto.getItemID());
                txitm.getSerialNumber().setValue(itmdto.getIdSerialNumber());
                txitm.getManufacturerItemUPC().setValue(itmdto.getManufacturerItemUPC());
                txitm.getRetailTransactionLineItemSequenceNumber().setValue(Long.valueOf(itmdto.getRetailTransactionLineItemSequenceNumber()).intValue());
                txitm.getSaleReturnLineItemQuantity().setValue(BigDecimal.valueOf(itmdto.getSaleReturnLineItemQuantity()));
                txitm.getRecordCreatedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));
                txitm.getRecordLastModifiedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));

                if (changeType.equals(ChangeType.Put)) {
                    dbtype = "insert";
                    inserted = localDb.addTransactionLineItem(txitm);
                }
                else if (changeType.equals(ChangeType.Update)) {
                    dbtype = "udpate";
                    inserted = localDb.updateRetailLineItem(txitm);
                }

                FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, type:{3}, table:{1}, id:{2}",
                                inserted,
                                txitm.getTableName(),
                                txitm.getTransactionSequenceNumber().getValue(),
                                dbtype);

            }

        }
        return true;
    }





    @Override
    public boolean onCheckpointComplete(long checkpointVersion) {
        //logger.log(format("onCheckpointComplete(checkpointVersion={0})", checkpointVersion), Level.INFO);
        return true;
    }





    @Override
    public void onCheckpointStart(long checkpointVersion) {
        //logger.log(format("onCheckpointStart(checkpointVersion={0})", checkpointVersion), Level.INFO);
    }





    @Override
    public void onLogComplete(int logNumber, LogCompletionReason reason, Throwable errorCause) {
        //logger.log(format("onLogComplete(logNumber={0})", logNumber), Level.INFO);
    }





    @Override
    public void onLogStart(int logNumber) {
        //logger.log(format("onLogStart(logNumber={0})", logNumber), Level.INFO);
    }





    @Override
    public void onWait() {}

}
