/****************************************************************************
 * FILE: MsgLogger.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.service.txmanager;

import com.freeingreturns.utils.FRLogger;
import com.neeve.trace.Tracer.Level;

public class MsgLogger {

    public void logReplyCall(long msgId, String channel, String strMsg) {
        FRLogger.logfmt(Level.INFO, msgId, channel, "Rcvd rply: {0}", strMsg);
    }





    public void logExtQryLookupFailed(long msgId, String channel, Class<?> type, String key) {
        FRLogger.logfmt(Level.INFO, msgId, channel, "ExternalQryLookup failed. Type: {0}, ID: {1}", type.getSimpleName(), key);
    }





    public void logIgnoredMessage(long msgId, String channel, Class<?> type) {
        FRLogger.logfmt(Level.INFO, msgId, channel, "IGNORED: {0}", type.getSimpleName());
    }

}
