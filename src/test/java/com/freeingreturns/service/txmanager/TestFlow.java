package com.freeingreturns.service.txmanager;

import static org.junit.Assert.assertEquals;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.freeingreturns.service.txmanager.driver.BasicFlow;

/**
 * A test case that tests the application flow. 
 */
public class TestFlow extends AbstractTest {

    /**
     * 
     */
    private Properties setEnv() {
        Properties env = new Properties();
        env.put("nv.ddl.profiles", "junit");
        env.put("x.apps.txm-processor.storage.clustering.enabled", "false");
        env.put("appdataroot", "target/testbed/rdat");
        return env;
    }

    /**
     * 
     */
    @Test
    public void testFlow() throws Throwable {
        int sendCount = 1;
        Properties env = setEnv();

        
        startApp(TransactionManagerMain.class, "txm-processor", "txm-instA", env);
        
        // start the send driver
        BasicFlow sendDriver = startApp(BasicFlow.class, "txm-basicflow", "txm-utils", env);
        sendDriver.doSend(1,100);
        CountDownLatch latch = sendDriver.getMsgLatch();
        if ( latch!=null ) {
            latch.await(10, TimeUnit.SECONDS);
            assertEquals("Have not processed all messages", 0, latch.getCount());
            return;
        }
        assert(false);
    }

}
